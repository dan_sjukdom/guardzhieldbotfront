import { firebaseConfig } from "../config/firebase.config";
import firebase from "firebase";


firebase.initializeApp(firebaseConfig); 


export default firebase;