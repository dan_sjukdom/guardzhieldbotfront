import axios from "axios";


export const createPGUser = async (user) => {
    try {
        const response = await axios.post("/api/user/actions", user);
        if (response.status === 200) {
            const data = response.data.data;
            return {
                success: true,
                error: null,
                data: data
            };
        }
        return {
            success: false,
            error: {
                message: response.error.message,
            },
            data: null
        }
    }
    catch (e) {
        return {
            success: false,
            error: {
                message: e.message
            },
            data: null
        };
    }
};