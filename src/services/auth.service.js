import firebase from "./firebase.service";
import axios from "axios";


export const Login = async (email, password) => {
    try {
        console.log("This is the email")
        console.log(email)
        const user = await firebase.auth().signInWithEmailAndPassword(email, password);
        console.log("Ussseeeer");
        console.log(user)
        if (user) {
            const token = await user.user.getIdToken();
            localStorage.setItem("token", token);
            return {
                success: true,
                error: null,
                data: token
            }
        }
        else {
            return {
                success: false,
                error: {
                    code: "",
                    message: "The user doesn't exist"
                },
                data: null
            }
        }
    }
    catch (e) {
        return {
            success: false,
            error: {
                code: e.code,
                message: e.message
            }
        }
    }
};


export const createUser = async (email, password) => {
    const data = {
        email: email,
        password: password
    };
    try {
        const request = await axios.post("/api/user", data);
        if (request.status === 200) {
            console.log(request.data)
            const data = JSON.parse(request.data);
            return {
                success: data.success,
                error: data.error,
                data: data.data
            }
        }
    }
    catch (e) {
        console.log(e.message)
        throw new Error(e.message);
    }
};