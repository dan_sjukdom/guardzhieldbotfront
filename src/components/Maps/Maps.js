import React, {
    useState,
    useContext,
} from 'react'
import { makeStyles } from "@material-ui/core";
import ReactMapGL from "react-map-gl";
import axios from "axios";
import { MapsContext } from "./MapsContext";


const useStyles = makeStyles(theme => ({
    mapbox: {
    }
}));


const Maps = () => {

    const classes = useStyles();
    const [state, dispatch] = useContext(MapsContext);

    const [viewport, setViewport] = useState({
        width: "100%",
        height: 800,
        latitude: 19.432608,
        longitude: -99.133209,
        zoom: 3
    });

    return (
        <ReactMapGL 
            className={classes.mapbox}
            {...viewport}
            mapboxApiAccessToken={"pk.eyJ1Ijoic2p1a2RvbSIsImEiOiJja2sxZmpqamowbWtzMm9xZHN4bjk1YWNjIn0.MP1qf3mpUT8eui_Dfg_zuA"}
            mapStyle={state.mapboxDarkTheme? "mapbox://styles/mapbox/dark-v10" : "mapbox://styles/mapbox/streets-v11"}
            onViewportChange={nextViewport => setViewport(nextViewport)}
        />
    )
};

export default Maps;