import React, { 
    createContext,
    useReducer
} from 'react';


const initialState = {
    longitude: 0.0,
    latitude: 0.0,
    direction: "",
    mapboxDarkTheme: false
};

const reducer = (state, action) => {
    switch(action.type) {
        case "SET_LONGITUDE":
            return {
                ...state,
                longitude: action.payload
            }
        case "SET_LATITUDE":
            return {
                ...state,
                latitude: action.payload
            }
        case "SET_DIRECTION":
            return {
                ...state,
                direction: action.payload
            }
        case "SET_MAPBOX_DARK_THEME":
            return {
                ...state,
                mapboxDarkTheme: action.payload
            }
    }
};


export const MapsContext = createContext();

export const MapsContextProvider = ({children}) => { 
    
    const [state, dispatch] = useReducer(reducer, initialState);
    
    return (
        <MapsContext.Provider value={[state, dispatch]}>
            {children}
        </MapsContext.Provider>
    )
};