import React from 'react'
import { MapsContextProvider } from "./MapsContext";
import Maps from "./Maps";
import Controls from "./Controls";


const MapsWrapper = () => {
    return (
        <MapsContextProvider>
            <Controls />
            <Maps />
        </MapsContextProvider>
    )
};

export default MapsWrapper;
