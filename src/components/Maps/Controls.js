import React, {
    useContext,
    useState
} from "react";
import {
    TextField,
    Switch,    
    makeStyles,
    FormControlLabel,
    Button
} from "@material-ui/core";
import { MapsContext } from "./MapsContext";
import { AuthContext } from "../Authentication/AuthContext";
import IncidentModal from "../IncidentForm/IncidentModal";


const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        margin: "20px 20px",
    },
    form: {
        width: "-webkit-fill-available"
    },

}));


const Searchbar = () => {
    const classes = useStyles();
    const [state, dispatch] = useContext(MapsContext);
    const [userState, userDispatch] = useContext(AuthContext)
    const [modalOpen, setModalOpen] = useState(state.isUserLogged);

    const handleClickButton = () => {
        if (userState.isUserLogged) {
            setModalOpen(true);
        }
        else {
            alert("Sign in to register an incident")
        }
    };

    console.log(userState)

    return (
        <div className={classes.root}>
            <form noValidate autoComplete="off" className={classes.form}>
                <TextField
                    label="Puedes escribir una direccion aqui"
                    style={{
                        width: "inherit",
                        marginBottom: "15px"
                    }}
                />
                    {/*<FormControlLabel
                        control={
                            <Switch 
                                checked={state.mapboxDarkTheme}
                                onChange={(e) => dispatch({
                                    type: "SET_MAPBOX_DARK_THEME",
                                    payload: !state.mapboxDarkTheme
                                })}
                            />
                        }
                        label="Dark theme"
                    />
                    */}
            </form>
            <Button
                variant="contained"
                color="primary"
                onClick={handleClickButton}
            >
                Report
            </Button>
            <IncidentModal open={modalOpen} setOpen={setModalOpen} />
        </div>
    )
};

export default Searchbar
