import React, { useState, useContext } from 'react'
import {
    AppBar, 
    Toolbar,
    Typography,
    IconButton,
    makeStyles,
    MenuList,
    MenuItem,
    Menu,
} from "@material-ui/core";
import {
    Link
} from "react-router-dom";
import HomeIcon from '@material-ui/icons/Home';
import ExploreIcon from '@material-ui/icons/Explore';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { AuthContext } from "../Authentication/AuthContext";


const useStyles = makeStyles(theme => ({
    appbar__root: {
        backgroundColor: "#212121"
    },
    navigation__title: {
        flexGrow: 1,
        fontFamily: "Roboto Mono, monospace"
    },
    navigation__links: {
        textDecoration: "none"
    },
    navigation__buttons: {
        color: "white"
    }
}));




const Navigation = () => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const [state, dispatch] = useContext(AuthContext);
    const isMenuOpen = Boolean(anchorEl);


    const handleUserMenuClose = () => {
        setAnchorEl(null);
    }


    const menuId = 'primary-search-account-menu';
    const renderUserMenu = (
        <Menu
           anchorEl={anchorEl}
           anchorOrigin={{ vertical: "top", horizontal: "right" }}
           id={menuId}
           open={isMenuOpen}
           onClose={handleUserMenuClose}
        >
        {
            state.isUserLogged? (
                <React.Fragment>
                    <Link to="/profile/:id" className={classes.navigation__menu}>
                        <MenuItem onClick={handleUserMenuClose}> Profile </MenuItem>
                    </Link>
                    <Link to="/logout" className={classes.navigation__menu}>
                        <MenuItem onClick={handleUserMenuClose}> Logout </MenuItem>
                    </Link>
                </React.Fragment>
            ) : ( 
                <React.Fragment>
                    <Link to="/login" className={classes.navigation__menu}>
                        <MenuItem onClick={handleUserMenuClose}> Login </MenuItem>
                    </Link>
                    <Link to="/register" className={classes.navigation__menu}>
                        <MenuItem onClick={handleUserMenuClose}> Register </MenuItem>
                    </Link>
                </React.Fragment>
            )
        }
        </Menu>
    );

    return (
        <div>
        <AppBar position="static" className={classes.appbar__root}>
            <Toolbar>
               <Typography variant="h4" className={classes.navigation__title}>
                  GuardBotMap
               </Typography> 
               <div className={classes.navigation__links}>
                    <IconButton>
                        <Link to="/" className={classes.navigation__buttons}>
                            <HomeIcon />
                        </Link>
                    </IconButton>
                    <IconButton>
                        <Link to="/maps" className={classes.navigation__buttons}>
                            <ExploreIcon /> </Link>
                    </IconButton>
                    <IconButton
                        aria-haspopup="true"
                        onClick={(e) => setAnchorEl(e.currentTarget)}
                    >
                        <AccountCircle style={{ color: "white" }}/>
                    </IconButton>
               </div>
            </Toolbar>
        </AppBar>
        { renderUserMenu }
        </div>
    )
}

export default Navigation
