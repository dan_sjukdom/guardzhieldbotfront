import React, { useContext, useState } from "react"; 
import { Redirect } from "react-router-dom";
import {
    Button,
    TextField,
    makeStyles
} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import { useForm, Controller, SubmitHandler} from "react-hook-form"; 
import { AuthContext } from "./AuthContext";
import { Login } from "../../services/auth.service";


const useStyles = makeStyles(theme => ({
    form: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    wrapper: {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
    },
    text_field: {
        margin: "20px"
    }
}));


const AuthForm = () => {
    const classes = useStyles();
    const [ state, dispatch ] = useContext(AuthContext);
    const [ error, setError] = useState(false);
    const [ redirect, setRedirect ] = useState(false);
    const methods = useForm();
    const { handleSubmit, control, reset, errors, register } = methods;


    const onSubmit = async data => {
        
        console.log("Submit")
        console.log(errors)

        const loginSuccess = await Login(data.email, data.password); 

        if (loginSuccess.success) {
            dispatch({
                type: "IS_USER_LOGGED",
                payload: true
            });
            setError(false);
            setRedirect(true);
        }
        else {
            setError(true);
        }
    };

    if (redirect) {
        return <Redirect to="/maps" />
    }


    return (
        <div className={classes.wrapper}>
        {
            error? (
                <Alert onClose={ () => { setError(false) }} severity="error">
                    The email or password are invalid
                </Alert>
            ) : null
        }
        <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
            <Controller
                name="email"
                control={control}
                defaultValue={""}
                rules={{ 
                    required: true,
                    //pattern: {
                    //    value: /[a-zA-Z_]+@([a-zA-Z]+\.)][a-zA-Z]+/,
                    //    message: "The input must a be valid email"
                    //    }
                 }}
                render={props =>
                    <TextField
                        name="email"
                        onChange={e => props.onChange(e.target.value)}
                        variant="outlined"
                        label="Email"
                        error={errors.email? true : null}
                        helperText={() => {
                            if(errors.email?.type === "required")
                                return "This field is required";
                            else
                                return null;
                        }}
                        className={classes.text_field}
                        ref={
                            register({
                                pattern: {
                                    value: /[a-zA-Z]+@([a-zA-Zz]+\.)][a-zA-Z]+/,
                                    message: "The input must a be valid email"
                                }
                            })
                        }
                    />
                }
           />
           {/*{ errors.Email?.type === "required" && "Your input is required" }*/}
           <Controller
                name="password"
                control={control}
                defaultValue={""}
                rules={{ required: true }}
                render={props =>
                
                    <TextField
                        name="password"
                        onChange={e => props.onChange(e.target.value)}
                        variant="outlined"
                        label="Password"
                        type="password"
                        error={errors.password? true : null}
                        helperText={errors.password?.type === "required"? "The password is required" : null}
                        className={classes.text_field}
                        ref={
                            register({
                                required: true,
                                minLength: {
                                    value: 8,
                                    message: "The password must be at least 8 characters"
                                },
                                maxLength: {
                                    value: 20, 
                                    message: "The password must be less than 20 character"
                                }
                            })
                        }
                    />
                }
            />
            <Button 
                name="SubmitButton"
                variant="contained" 
                color="primary"
                style={{ margin: "20px" }}
                type="submit"
            >
                Submit
            </Button>
        </form>
        </div>
    )
};

export default AuthForm;
