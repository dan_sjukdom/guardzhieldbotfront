import { createContext, useReducer } from "react";


const reducer = (state, action) => {
    switch(action.type) {
        case "SET_USER_TOKEN": {
            const token = action.payload;
            localStorage.setItem("token", token)
            return {
                ...state,
                token: action.payload
            }
        }
        case "DELETE_USER_TOKEN": {
            localStorage.removeItem("token");
            return {
                ...state,
                token: ""
            }
        }
        case "IS_USER_LOGGED": {
            return {
                ...state,
                isUserLogged: action.payload
            }
        }
    }
};

const inititalState = {
    token: "",
    isUserLogged: false
};




export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const [ state, dispatch ] = useReducer(reducer, inititalState);
    return(
        <AuthContext.Provider value={[state, dispatch]}>
            { children }
        </AuthContext.Provider>
    )
};
