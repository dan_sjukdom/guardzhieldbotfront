import React from 'react';
import {
    Button,
    Typography,
    makeStyles
} from "@material-ui/core";


const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
    },
    home__title: {
        margin: "10px",
        textAlign: "center"
    },
    home__description: {
        margin: "10px",
        textAlign: "center"
    },
    button__root: {
        margin: "10px",
        backgroundColor: "#212121",
    },
    button__label: {
        color: "white"
    },
    button__focus_visible: {
        backgroundColor: "#212121",
        color: "white"
    }
}));



const Home = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Typography variant="h2" className={classes.home__title}>
                GuardBot Maps
            </Typography>
            <Typography variant="h6" className={classes.home__description}>
                Bienvenido a los mapas de GuardBot. Aquí encontrarás en el mapa las distintas incidencias reportadas en México referentes a accidentes, asaltos, robos y violencia.
            </Typography>
            <Button 
                variant="contained"
                classes={{
                    root: classes.button__root,
                    label: classes.button__label,
                    focusVisible: classes.button__focus_visible
                }}
            >
                Login
            </Button>
            <Button 
                variant="contained"
                classes={{
                    root: classes.button__root,
                    label: classes.button__label,
                    focusVisible: classes.button__focus_visible
                }}
            >
                Signup
            </Button>
        </div>
    )
};


export default Home;