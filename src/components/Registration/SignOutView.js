import { AuthContext } from "../Authentication/AuthContext"; 
import { useContext } from "react";
import { Redirect } from "react-router-dom";
import {
    Grid,
    Container
} from "@material-ui/core";


const SignOutView = () => {
    const [state, dispatch] = useContext(AuthContext);

    console.log(state)

    if (state.isUserLogged) {
        dispatch({
            type: "IS_USER_LOGGED",
            payload: false
        });
        dispatch({
            type: "DELETE_USER_TOKEN",
        });
    }

    return (
        <Container>
            <h1> Thank you for helping us! </h1>
        </Container>
    )
};


export default SignOutView;