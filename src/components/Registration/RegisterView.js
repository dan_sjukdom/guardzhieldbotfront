import React, { useState } from "react";
import { 
    TextField,
    Button,
    makeStyles
} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import { useForm, Controller } from "react-hook-form";
import { Redirect } from "react-router-dom";
import { createUser } from "../../services/auth.service";
import { createPGUser } from "../../services/postgres.service";


const useStyles = makeStyles(theme => ({
    form: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    wrapper: {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
    },
    textField: {
        margin: "10px"
    }
}));


const RegisterView = () => {
    const classes = useStyles();
    const methods = useForm();
    const { handleSubmit, control, reset, errors, register } = methods;
    const [ alert, setAlert ] = useState({
        message: "",
        active: false,
        type: ""
    });
    const [ redirect, setRedirect ] = useState(false);
    

    console.log("Redirect?")
    console.log(redirect)

    const onSubmit = async data => {

        if (data.password1 !== data.password2) {
            setAlert({
                active: true,
                message: "The passwords doesn't match",
                type: "error"
            });
            return;
        }
        if (!(data.password1.length >= 8 && data.password1.length <= 20)) {
            setAlert({
                active: true,
                message: "The password length must be between 8 and 20",
                type: "error"
            });
            return;
        }
        
        try {
            let response = await createUser(data.email, data.password1);        
            if (response.success) {
                setAlert({
                    message: "User created successfully",
                    active: true,
                    type: "success"
                });
                response = await createPGUser({ email: data.email });  
                console.log(response)
                //setRedirect(true);
            }
            else {
                setAlert({
                    message: response.error.message,
                    active: true,
                    type: "error"
                });
            }
        }
        catch (e) {
            setAlert({...alert, message: e.message, active: true})
        }
    }
    
    if (redirect) {
        return <Redirect to="/login" />
    }
    
    return (
        <div className={classes.wrapper}>

            {
                alert.active? (
                    <Alert onClose={ () => setAlert({...alert, active: false})} severity={alert.type} style={{ margin: "10px" }}>
                        { alert.message }
                    </Alert>
                ) : null 
            }

            <form autoComplete="off" onSubmit={handleSubmit(onSubmit)} className={classes.form}>
                <Controller
                    name="email"
                    control={control}
                    defaultValue={""}
                    rules={{
                        required: true
                    }}
                    render={props => 
                        <TextField
                            name="emailField"       
                            onChange={(e) => props.onChange(e.target.value)}
                            variant="outlined"
                            label="Email"
                            error={errors.email? true : null}
                            helperText={errors.email?  "Make sure to enter a valid mail address" : null}
                            className={classes.textField}
                        />
                    }                   
                />

                <Controller
                    name="password1"
                    control={control}
                    defaultValue={""}
                    rules={{
                        required: true
                    }}
                    render={props => 
                        <TextField
                            name="password1"       
                            onChange={(e) => props.onChange(e.target.value)}
                            variant="outlined"
                            label="Password 1"
                            type="password"
                            error={errors.password1? true : null}
                            helperText={errors.password1?  "Make sure to enter a valid mail address" : null}
                            className={classes.textField}
                        />
                    }                   
                />

                <Controller
                    name="password2"
                    control={control}
                    defaultValue={""}
                    rules={{
                        required: true
                    }}
                    render={props => 
                        <TextField
                            name="password2"       
                            onChange={(e) => props.onChange(e.target.value)}
                            variant="outlined"
                            label="Password 2"
                            type="password"
                            error={errors.password2? true : null}
                            helperText={errors.password2?  "Make sure to enter a valid mail address" : null}
                            className={classes.textField}
                        />
                    }                   
                />

                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                >
                    Register
                </Button>
            </form>
        </div>
    )
};


export default RegisterView;