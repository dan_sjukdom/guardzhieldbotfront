import { useState } from "react";
import {
    TextField,
    makeStyles,
    Button
} from "@material-ui/core";
import {
    useForm,
    Controller
} from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";


const useStyles = makeStyles(theme => ({
    form: {

    },
    text_field: {
        margin: "20px"
    }
}));


const schema = yup.object().shape({
    address: yup.string().min(10).max(100).required(),
    description: yup.string().min(30).max(400).required()
})


const IncidentModal = () => {

    const classes = useStyles();
    const [error, setError] = useState(false);
    const methods = useForm({
        resolver: yupResolver(schema)
    });
    const { handleSubmit, control, reset, errors } =  methods;
    

    console.log("Errrors")
    console.log(errors)

    const onSubmit = async data => {
       /**
        * Create the location entry if it doesn't exist.
        * Then create the report document and then 
        * join them via foreing key.
        */ 
       console.log("FORM DATA")
       console.log(data)
    };

    return (
            <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
                <Controller
                    name="address"
                    control={control}
                    defaultValue={""}
                    rules={{
                        required: true
                    }}
                    render={props => 
                        <TextField
                            name="address"
                            variant="outlined"
                            label="Address"
                            error={errors.address? true : false}
                            helperText={errors.address? errors.address.message : null}
                            className={classes.text_field}
                            style={{
                                width: "-webkit-fill-available"
                            }}
                            onChange={(e) => props.onChange(e.target.value)}
                        /> 
                    }
                />
                <Controller
                    name="description"  
                    control={control}
                    defaultValue={""}
                    rules={{
                        required: true,
                        maxLength: 10
                    }}
                    render={props => 
                        <TextField
                            name="description"
                            variant="outlined"
                            label="Description"
                            error={errors.description? true : false}
                            helperText={errors.description? errors.description.message : null}
                            className={classes.text_field}
                            multiline
                            rows={5}
                            style={{
                                width: "-webkit-fill-available"
                            }}
                            onChange={(e) => props.onChange(e.target.value)}
                        /> 
                    }
                />
                <Button
                    name="SubmitButton"
                    type="submit"
                    variant="contained"
                    color="primary"
                >
                    Submit
                </Button>
            </form>
    )
};


export default IncidentModal;