import { Dialog, 
    DialogTitle, 
    DialogContent, 
    makeStyles,
    Typography,
    IconButton
} from "@material-ui/core";
import IncidentForm from "./IncidentForm";
import CloseIcon from '@material-ui/icons/Close';


const useStyles = makeStyles(theme => ({
    dialog: {

    },
    dialog_title__wrapper: {
        display: "flex"
    }
}));


const IncidentModal = ({ open, setOpen }) => {
   
    const classes = useStyles();
    
    return (
        <Dialog
            open={open}
            onClose={() => setOpen(false)}
            maxWidth="md"
            fullWidth={true}
            className={classes.dialog}
        >
            <DialogTitle>
                <div className={classes.dialog_title__wrapper}>
                    <Typography
                        variant="h5" 
                        style={{ flexGrow: 1 }}
                    >
                        Report an incident
                    </Typography>
                    <IconButton
                        variant="contained" 
                        color="secondary"
                        onClick={() => setOpen(false)}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
            </DialogTitle>
            <DialogContent>
                <IncidentForm />
            </DialogContent>
        </Dialog> 
    )
}

export default IncidentModal;