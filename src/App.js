import {
  BrowserRouter as Router,
  HashRouter as HRouter,
  Route,
  Switch
} from "react-router-dom";
import Home from "./components/Home/Home";
import Navigation from "./components/Navigation/Navigation";
import MapsWrapper from "./components/Maps/MapsWrapper";
import AuthView from "./components/Authentication/AuthView";
import RegisterView from "./components/Registration/RegisterView";
import SignOutView from "./components/Registration/SignOutView";
import "./assets/css/App.css";
import { AuthProvider } from "./components/Authentication/AuthContext";


function App() {
  return (
    <HRouter>
      <AuthProvider>
      <Navigation />
        <Switch> 
          <Route exact path="/" component={Home} />
          <Route exact path="/register" component={RegisterView} />
          <Route exact path="/login" component={AuthView} />
          <Route exact path="/maps" component={MapsWrapper} />
          <Route excact path="/logout" component={SignOutView} />
        </Switch>
      </AuthProvider>
    </HRouter>
  );
}

export default App;
